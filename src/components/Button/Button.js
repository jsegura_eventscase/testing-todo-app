import React from "react";
import styles from "./Button.module.scss";
export const Button = (props) => {
  const handleSubmit = (e) => {
    e.preventDefault();
    props.setAllTasks([...props.allTasks, props.newTaskText]);
  };

  return (
    <button type="submit" className={styles.button} onClick={handleSubmit}>
      Add task
    </button>
  );
};
