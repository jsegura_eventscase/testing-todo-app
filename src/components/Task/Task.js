import React from "react";
import styles from "./Task.module.scss"

export const Task = ({text}) => {
  return <p className={styles.task}>{text}</p>;
};