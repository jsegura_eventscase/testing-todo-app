import React from "react";
import styles from "./NewTask.module.scss";

export const NewTask = (props) => {
  const handleChange = (e) => {
    props.setNewTaskText(e.target.value);
  };

  return (
    <div className={styles.container}>
      <label for={"new-task"} classname={styles}>New task: </label>
      <textarea
        id={"new-task"}
        className={styles.textarea}
        value={props.newTaskText}
        onChange={handleChange}
      />
    </div>
  );
};
