import React, { useState } from "react";
import { Task } from "../Task/Task";
import { NewTask } from "../NewTask/NewTask";
import { Button } from "../Button/Button";
import styles from "./TodoList.module.scss";

export const TodoList = () => {
  const [allTasks, setAllTasks] = useState(["Go to gym", "Clean dishes"]);
  const [newTaskText, setNewTaskText] = useState("");

  return (
    <form className={styles.listContainer}>
      <h2 className={styles.title}>Todo List</h2>
      {allTasks.map((el) => {
        return <Task text={el} key={el} />;
      })}
      <NewTask newTaskText={newTaskText} setNewTaskText={setNewTaskText} />
      <Button
        allTasks={allTasks}
        setAllTasks={setAllTasks}
        newTaskText={newTaskText}
      />
    </form>
  );
};
